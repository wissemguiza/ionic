var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { UserService } from '../api/user.service';
import { Utilisateur } from '../entities/utilisateur.entities';
var LoginPage = /** @class */ (function () {
    function LoginPage(utilisateurService) {
        this.utilisateurService = utilisateurService;
        this.utilisateur = new Utilisateur();
    }
    LoginPage.prototype.login = function () {
        this.navCtrl.navigateForward('/home');
    };
    LoginPage.prototype.inscription = function () {
        this.navCtrl.navigateForward('/inscription');
    };
    LoginPage.prototype.loginForms = function () {
        this.utilisateurService.login(this.utilisateur);
        console.log(this.utilisateur.nomUtilisateur);
    };
    LoginPage.prototype.ngOnInit = function () {
        var _this = this;
        this.utilisateurService.getUsers().subscribe(function (data) {
            _this.utilisateurs = data;
        });
    };
    LoginPage = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        __metadata("design:paramtypes", [UserService])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map