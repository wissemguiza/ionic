var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { UserService } from '../api/user.service';
import { Utilisateur } from '../entities/utilisateur.entities';
var InscriptionPage = /** @class */ (function () {
    function InscriptionPage(utilisateurService) {
        this.utilisateurService = utilisateurService;
        this.utilisateur = new Utilisateur();
        console.log("Hello");
    }
    InscriptionPage.prototype.ngOnInit = function () {
        /*this.utilisateurService.ajouter_client().subscribe( data => {
        this.utilisateurs = data;
      })*/
    };
    InscriptionPage.prototype.logForms = function () {
        this.utilisateurService.ajouterClient(this.utilisateur);
        console.log(this.utilisateur.nomUtilisateur);
    };
    InscriptionPage = __decorate([
        Component({
            selector: 'app-inscription',
            templateUrl: './inscription.page.html',
            styleUrls: ['./inscription.page.scss'],
        }),
        __metadata("design:paramtypes", [UserService])
    ], InscriptionPage);
    return InscriptionPage;
}());
export { InscriptionPage };
//# sourceMappingURL=inscription.page.js.map