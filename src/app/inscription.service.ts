import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";


export class Utilisateur{
  constructor(
    idUtilisateur: number,
    pseudoUtilisateur: string,
    MotDePasseUtilisateur: string,
    nomUtilisateur: String,
    preomUtilisateur: string,
    adresseUtilisateur: string,
    adresseMailUtilisateur: string,
    naissanceUtilisateur: Date,
    telUtilisateur: String,
  ) {}
  
}

@Injectable()
export class InscriptionService {
  [x: string]: any;

  constructor(private http: HttpClient) { }
  baseUrl: string ='//localhost:8087/v1/utilisateur/';
  
 
  
 
  public createUser(utilisateur) {
    return this.http.post<Utilisateur>("http://localhost:8087/api/v1/client", utilisateur);
  }
  public update(utilisateur): Observable<any>{
    return this.http.put(this.url, utilisateur);
  }
}

 

