import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { InscriptionPage } from '../inscription/inscription.page';
import { Observable } from 'rxjs';
import { Utilisateur } from '../entities/utilisateur.entities';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HTTP } from '@ionic-native/http/ngx'
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  credentials = {
    username: '',
    password: ''
  };
  requestObject: any = null;
  
  utilisateur: Utilisateur = new Utilisateur();

  utilisateurs:Utilisateur[];
  navCtrl: any;

  constructor(private fb: FormBuilder, private appService: AppService, private router:Router) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
    });
  }
 
  login(){
    this.appService.authenticate(this.credentials, ()=>{
      this.router.navigateByUrl('/home');
    });
  }
  
   /*
   loginForms(){
    this.utilisateurService.login(this.utilisateur);
    console.log(this.utilisateur.nomUtilisateur);
  }
  */
 

  
  
  }

  

    
  
