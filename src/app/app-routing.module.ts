import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageModule } from './login/login.module';
import { LoginPage } from './login/login.page';
import { InscriptionPage } from './inscription/inscription.page';
import { InscriptionPageModule } from './inscription/inscription.module';
import { HomePage } from './home/home.page';
import { SettingsPage } from './settings/settings.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'inscription',
    component: InscriptionPage
  },
  {
    path: 'home',
    component: HomePage
  },
  {
    path: 'settings',
    component: SettingsPage
  },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
 
 
    
  

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
