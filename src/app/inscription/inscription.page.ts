import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';
import { InscriptionService, Utilisateur } from '../inscription.service';




@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  


  constructor(private fb: FormBuilder,private router: Router, private InscriptionService: InscriptionService) { }
  InscriptionForm: FormGroup;

  date: Date ;
  utilisateur = new Utilisateur(0,"","","","","","",this.date,"");


  
  ngOnInit() {this.InscriptionForm = this.fb.group({
    id: [],
    pseudoUtilisateur: ['', Validators.required],
    MotDePasseUtilisateur: ['', Validators.required],
    nomUtilisateur: ['', Validators.required],
    preomUtilisateur: ['', Validators.required],
    adresseUtilisateur: ['', Validators.required],
    adresseMailUtilisateur: ['', Validators.required],
    naissanceUtilisateur: ['', Validators.required],
    telUtilisateur: ['', Validators.required]});
  }

  /*
      
    createEmployee(Utilisateur): void {
      this.InscriptionService.createUser(this.utilisateur)
          .subscribe( data => {
            alert("User created successfully.");
          });
    };*/
    
  
      onSubmit(Utilisateur) {
        this.InscriptionService.createUser(this.utilisateur)
        .subscribe( data => {
          alert("User created successfully.");
        });
        
        }
      }
    
    
    
    



