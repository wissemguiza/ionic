const BASE = 'http://localhost:';

const PORT = 8087;
const PATH = '/api';

export const API_URLS = {

  USER_URL: BASE + PORT + PATH +'/user',
  GERANT_URL: BASE + PORT + '/v1/utilisateur'
};
