import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from './app.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { XhrInterceptor } from './xhr.interceptor';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { LoginPage } from './login/login.page';
import {HttpModule} from '@angular/http';
import { InscriptionPage } from './inscription/inscription.page';
import { IonicStorageModule } from '@ionic/storage';
import { HomePage } from './home/home.page';
import { principalReducer } from './shared/principal.reducer';
import { InscriptionService } from './inscription.service';
import { SettingsPage } from './settings/settings.page';






@NgModule({
  declarations: [AppComponent , LoginPage, InscriptionPage, HomePage, SettingsPage],
  entryComponents: [],
  providers: [
    CookieService,
    HttpClientModule,
    AppService,
    InscriptionService,
    StatusBar,
    SplashScreen,
    { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
   IonicModule.forRoot(),
   StoreModule.forRoot({principal: principalReducer})
  ],
  schemas:      [ CUSTOM_ELEMENTS_SCHEMA ],
  
  bootstrap: [AppComponent]
})  
export class AppModule {
  
}
